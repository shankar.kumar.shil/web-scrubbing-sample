package com.dheeraj.webscrabing;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import au.com.bytecode.opencsv.CSVWriter;

public class CSBCKeyword {
	FirefoxDriver wd;

	@BeforeMethod
	public void setUp() throws Exception {
		wd = new FirefoxDriver();
		wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws IOException {
		wd.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		wd.get("http://cbseaff.nic.in/cbse_aff/schdir_Report/userview.aspx");
		wd.findElement(By.cssSelector("fieldset")).click();
		if (!wd.findElement(By.id("optlist_0")).isSelected()) {
			wd.findElement(By.id("optlist_0")).click();
		}
		wd.findElement(By.id("keytext")).click();
		wd.findElement(By.id("keytext")).clear();
		wd.findElement(By.id("keytext")).sendKeys("kolkata");
		wd.findElement(By.id("search")).click();
		// assertTrue((wd.findElements(By.id()).size() != 0));

		String tot_count_label = wd.findElement(By.id("lbltot")).getText();
		int tot_count = Integer.parseInt(tot_count_label);

		//List<String[]> schooldatabase = new ArrayList<String[]>();
		String csv = "/home/user/workspace/webscrabing/src/main/java/com/dheeraj/webscrabing/output.csv";
		CSVWriter writer = new CSVWriter(new FileWriter(csv),'\t');

		int iteration_count = 0;
		while (iteration_count < tot_count) {
			int row_count = wd.findElements(By.xpath(".//*[@id='T1']/tbody/tr/td/table")).size();
			System.out.println(row_count);

			int col_count = wd.findElements(By.xpath(".//*[@id='T1']/tbody/tr/td/table[2]/tbody/tr/td")).size();
			System.out.println(col_count);

			// divided xpath In three parts to pass Row_count and Col_count
			// values.
			String first_part = ".//*[@id='T1']/tbody/tr/td/table[";
			String second_part = "]/tbody/tr/td[";
			String third_part = "]";
			// Used for loop for number of rows.
			for (int i = 1; i <= row_count; i++) {
				String rowString = "";
				// Used for loop for number of columns.
				for (int j = 1; j <= col_count; j++) {
					// Prepared final xpath of specific cell as per values of i
					// and
					// j.
					String final_xpath = first_part + i + second_part + j + third_part;
					// Will retrieve value from located cell and print It.
					String Table_data = wd.findElement(By.xpath(final_xpath)).getText();
					System.out.print(Table_data + ";");
					rowString = rowString + Table_data + "\t";

				}
				System.out.println("");
				System.out.println("");
				System.out.println(rowString);
				rowString=rowString.substring(0, (rowString.length()-1));
				writer.writeNext(new String[] { rowString });
				//schooldatabase.add(new String[] { rowString });

				System.out.println("----------------------------------------");
			}
			wd.findElement(By.id("Button1")).click();
			iteration_count += row_count;

		}

		//writer.writeAll(schooldatabase);
		writer.close();

	}

	@AfterMethod
	public void tearDown() {
		wd.quit();
	}

	public static boolean isAlertPresent(FirefoxDriver wd) {
		try {
			wd.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}
}